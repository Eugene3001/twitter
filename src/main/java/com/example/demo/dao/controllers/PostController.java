package com.example.demo.dao.controllers;

import com.example.demo.dao.model.Post;
import com.example.demo.dao.model.exception.ResourceNotFoundException;
import com.example.demo.dao.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/posts")
@RestController
public class PostController {

    private final PostRepository postRepository;

    @Autowired
    public PostController(PostRepository postRepository) { this.postRepository = postRepository; }

    @GetMapping
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Iterable<Post> list() {
        return postRepository.findAll();
    }

    @GetMapping("/{id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Post get(@PathVariable Long id) throws ResourceNotFoundException {
        Post response = postRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        System.out.println("Have a nice day!");
        return response;
    }

    @PostMapping
    @ResponseBody
    @ResponseStatus(code = HttpStatus.CREATED)
    public Post post(@RequestBody Post request) {
        postRepository.save(request);
        return request;
    }

    @PutMapping("/{id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Post put(@RequestBody Post request, @PathVariable(name = "id") Long id) throws ResourceNotFoundException{
        Post response = postRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        response.setText(request.getText());
        response.setTitle(request.getTitle());
        response = postRepository.save(response);
        return  response;
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Post delete(@PathVariable Long id) throws ResourceNotFoundException {
        Post response = postRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        postRepository.delete(response);
        return response;
    }
}