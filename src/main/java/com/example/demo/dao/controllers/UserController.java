package com.example.demo.dao.controllers;

import com.example.demo.dao.model.User;
import com.example.demo.dao.model.exception.ResourceNotFoundException;
import com.example.demo.dao.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/users")
@Slf4j
public class UserController {
    private UserRepository userRepository;

    @Autowired // спринг связывает контроллер и репозиторию
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Iterable<User> list() {
        Iterable<User> all = userRepository.findAll();
        return all;
    }

    @GetMapping("/{id}")
    @ResponseBody
    public User get(@PathVariable Long id) throws Exception {
        Optional<User> byId = userRepository.findById(id);
        if (byId.isPresent()) {
            return byId.get();
        }

        throw new Exception("Not found");
    }

    @PostMapping
    @ResponseBody
    @ResponseStatus(code = HttpStatus.CREATED)
    public User create(@RequestBody User user) {
        userRepository.save(user);
        return user;
    }

    @PutMapping("/{id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public User update(@RequestBody User user, @PathVariable Long id) throws Exception {
        User byId = userRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        byId.setUserName(user.getUserName());
        byId.setPassword(user.getPassword());
        byId.setLastName(user.getLastName());
        byId.setFirstName(user.getFirstName());
        byId.setEmail(user.getEmail());
        return byId;
    }

    @GetMapping(params = {"ageGreaterThan"})
    @ResponseBody
    public List<User> listAgeGreaterThan(@RequestParam(name = "ageGreaterThan") Integer age) {
        List<User> response = userRepository.findAllByAgeGreaterThan(age);
        log.info("Get by age greater than param {}", age);
        return response;
    }
}