package com.example.demo.dao.repository;

import com.example.demo.dao.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findAllByAgeGreaterThan(Integer age);
}
